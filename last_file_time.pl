#!/usr/bin/perl -w
# This script shows the seconds since the epoch of the newest file.
use strict;
use Cwd;
use List::Util "max";
my $dir = cwd;
my $file;
my @times;
# Hele directory verwerken
opendir(DIR, $dir ) or die "can't open dir ".$dir.": $!";
FILE: while (defined($file = readdir(DIR))){
	# Skip the files that aren't interesting
	if (-f $file && $file ne '.listing' && $file ne 'update_is_running.lock' && $file ne 'newest_date.txt'){
		# Get file information
		my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($file);
		# Add the date to the array
		push (@times, $mtime);
	}
}
closedir(DIR);
# Print the maximum, being the newest date
print (max @times);
