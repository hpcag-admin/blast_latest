#!/bin/bash
# This script fetches taxonomy databases from ncbi
# written by Jan van Haarst, PRI
# needed : wget, curl & perl
# Variables
# Get location of this script, so we can call the last_file_time script
DIRNAME=`dirname $0`
SCRIPT_LOCATION=`perl -e 'END {use Cwd qw(realpath);print realpath($ARGV[0]);}' $DIRNAME`


site_location="ftp://ftp.ncbi.nih.gov/pub/UniVec/"
data_dir="/mnt/nexenta/reference/blast_latest/univec.nobackup"
extracted_dir=$data_dir"/extracted_latest.nobackup"
# Remove secondary checkfile
rm -f $data_dir"/newest_date.txt"
WGET="wget"
CURL="curl -s"
TAR="tar"
PRIMARY="true"

now=`date '+%Y_%m_%d_%A_%Hh%M'`
log_dir=$data_dir"/logs"
logfile=$log_dir"/univeclog_"$now
lockfile=$data_dir"/update_is_running.lock"
accept_list=""
reject_list="*.zip,*.Z,newest_date.txt"
wget_options="--proxy=off --mirror --no-parent --no-directories --level=1 --timestamping --passive-ftp --accept="$accept_list" -o "$logfile" --reject="$reject_list
tar_options="--use-compress-program=pigz --keep-newer-files --directory=${extracted_dir} -xvf"
# make the needed directories

mkdir -p $data_dir
mkdir -p $log_dir
mkdir -p $extracted_dir

# change to datadir
cd $data_dir

# catch sigint and remove lockfile

remove_lock()
{
if test -e $lockfile
    then
    # remove the update file.
    rm $lockfile
    exit
fi
}

trap remove_lock SIGINT SIGQUIT SIGTERM ERR

# fetch the data
if test -e $lockfile
    then
	# update is running, so print a message and stop.
	echo "Update is already running."
	exit
    else
	# lockfile does NOT exist, so we create the lockfile, run the update and remove the file.
	touch $lockfile
	# get the data
	$WGET  $wget_options  $site_location
	# Add return value to log file
	echo "Return Value="$? >> $logfile
	# Generate a timestamp, so we know what date the latest file was added
	perl $SCRIPT_LOCATION"/last_file_time.pl" > $data_dir"/newest_date.txt"

	# remove the lockfile.
	rm $lockfile
	# done
	exit
fi

