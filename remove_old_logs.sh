#!/bin/bash
# Remove logs that are older than a month from the blast getter tree.
find /mnt/nexenta/reference/blast_latest -type f -wholename '*logs/blast*' -ctime +30 -exec rm -v {} \;
find /mnt/nexenta/reference/blast_latest -type f -wholename '*logs/taxlog*' -ctime +30 -exec rm -v {} \;
